package com.hendisantika.centralize.swagger.doc.personapplication.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 * Project : person-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/10/18
 * Time: 08.12
 * To change this template use File | Settings | File Templates.
 */

@Configuration
public class SwaggerDocumentationConfiguration {
//    ApiInfo apiInfo() {
//        return new ApiInfoBuilder().title("Person REST CRUD operations API in Spring-Boot 2")
//                .description(
//                        "Sample REST API for centalized documentation using Spring Boot and spring-fox swagger 2 ")
//                .termsOfServiceUrl("").version("0.0.1-SNAPSHOT").contact(new Contact("Hendi Santika", "https://github.com/hendisantika",
//                "hendisantika@yahoo.co.id")).build();
//    }
//
//    @Bean
//    public Docket configureControllerPackageAndConvertors() {
//        return new Docket(DocumentationType.SWAGGER_2).select()
//                .apis(RequestHandlerSelectors.basePackage("com.hendisantika.centralize.swagger.doc
//                .personapplication")).build()
//                .apiInfo(apiInfo());
//    }

    @Bean
    public OpenAPI customOpenAPI(@Value("${application-description}") String appDesciption, @Value("${application" +
            "-version}") String appVersion) {
        return new OpenAPI()
                .info(new Info()
                        .title("Sample REST API for centalized documentation using Spring Boot and spring-fox swagger" +
                                " 3")
                        .version(appVersion)
                        .description(appDesciption)
                        .termsOfService("http://swagger.io/terms/")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org")));

    }

}
