package com.hendisantika.centralize.swagger.doc.personapplication.controller;

import com.hendisantika.centralize.swagger.doc.personapplication.entity.Person;
import com.hendisantika.centralize.swagger.doc.personapplication.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : person-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/10/18
 * Time: 08.17
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping(value = "/person")
public class PersonRestController {


    //Ideally you shall be using Service classes
    @Autowired
    PersonRepository personRepo;


    @GetMapping
    public ResponseEntity<List<Person>> getAllPersons() {
        return ResponseEntity.ok(personRepo.findAll());
    }

    @GetMapping("/{personid}")
    public ResponseEntity<Person> getPersonById(@PathVariable("personId") int personID) {
        Optional<Person> personInDB = personRepo.findById(personID);
        if (personInDB.isPresent()) {
            return ResponseEntity.ok(personInDB.get());
        } else {
            return new ResponseEntity<Person>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Person> storePerson(@RequestBody Person person) {
        Person personInDB = personRepo.save(person);
        return new ResponseEntity<Person>(personInDB, HttpStatus.CREATED);
    }

    @PutMapping("/{personId}")
    public ResponseEntity<Person> updatePersonDetails(@PathVariable("personId") int personID, @RequestBody(required = true) Person personDataToBeUpdated) {

        if (personID != personDataToBeUpdated.getId()) {    //Just to make sure we have same person_id in path param and body.
            return new ResponseEntity<Person>(HttpStatus.BAD_REQUEST);
        }

        Optional<Person> personInDB = personRepo.findById(personID);
        if (personInDB.isPresent()) {
            Person person = personRepo.saveAndFlush(personDataToBeUpdated);
            return ResponseEntity.ok(person);
        } else {
            return new ResponseEntity<Person>(HttpStatus.NOT_FOUND);
        }
    }
}
