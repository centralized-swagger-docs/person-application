package com.hendisantika.centralize.swagger.doc.personapplication.repository;

import com.hendisantika.centralize.swagger.doc.personapplication.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : person-application
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/10/18
 * Time: 08.15
 * To change this template use File | Settings | File Templates.
 */
public interface PersonRepository extends JpaRepository<Person, Integer> {
}